import { MyfantasyPage } from './app.po';

describe('myfantasy App', function() {
  let page: MyfantasyPage;

  beforeEach(() => {
    page = new MyfantasyPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
